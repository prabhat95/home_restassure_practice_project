import static io.restassured.RestAssured.given;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Grade {

	String head = "Medical Reimbursment";

	@Test( description = "Add new Grade Header!", priority = 1)
	public void Create_New_Gread_Head() {

		RestAssured.baseURI = "http://164.52.216.34:8085";

		String response = given().header("Content-Type", "application/json")
				.header("Authorization", "Basic VGVzdF9tZXJjaGFudDpUZXN0QDEyMw==")
				.body("{\"mid\":\"6irdwDznArIvUbS3POLsqg==\",\"headerName\":\"" + head + "\"}").log().all().when()
				.post("getepayPortal/eproRest/eproSaveGradeHeader").then().log().all().assertThat().statusCode(200)
				.extract().response().asString();

//		System.out.println(response);

		JsonPath js = new JsonPath(response);

		String status = js.getString("status");
		String toast = js.getString("message");

		System.out.println("Status displayed : " + status + " " + toast);

	}

	String id = null;

	// dataProvider = "addNewGradeHead",

	@Test(description = "Get All Header List!!", priority = 2)
	public void getHeaderList() {

		RestAssured.baseURI = "http://164.52.216.34:8085";
		String response = given().header("Content-Type", "application/json")
				.header("Authorization", "Basic VGVzdF9tZXJjaGFudDpUZXN0QDEyMw==")
				.queryParam("mid", "6irdwDznArIvUbS3POLsqg==").when().get("getepayPortal/eproRest/eproGradeHeader")
				.then().assertThat().statusCode(200).extract().response().asString();

		JsonPath js = new JsonPath(response);
		int count = js.getInt("data.size()");

		for (int i = 0; i < count; i++) {

			Object object = js.get("data[" + i + "].headerName");

			if (object.toString().equalsIgnoreCase(head.toString())) {
				id = js.getString("data[" + i + "].id");
				System.out.println(i + ". " + id);
			}

		}

	}

	@Test(priority = 3)
	public void delete_grade_head() {

		RestAssured.baseURI = "http://164.52.216.34:8085";

		String response = given().header("Content-Type", "application/json")
				.header("Authorization", "Basic VGVzdF9tZXJjaGFudDpUZXN0QDEyMw==").body("{\"ids\":[\"" + id + "\"]}")
				.when().post("getepayPortal/eproRest/eprodeleteGradeHeader").then().assertThat().statusCode(200)
				.extract().response().asString();

		JsonPath js = new JsonPath(response);

		String status = js.getString("status");
		String toast = js.getString("message");

		System.out.println("Status displayed : " + status + "  Toast Displayed : " + toast);

	}

}
