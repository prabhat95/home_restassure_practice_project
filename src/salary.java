import static io.restassured.RestAssured.given;

import java.io.FileNotFoundException;

import org.testng.annotations.Test;

import Resources.Utils;
import io.restassured.RestAssured;

public class salary {

	@Test
	public void salary() throws FileNotFoundException {

		RestAssured.baseURI = "http://164.52.216.34:8085";

		String response = given().spec(Utils.requestSpecification()).queryParam("eid", "890")
				.queryParam("mid", "xtFGO11w3whEwKvVNLWHrw==").queryParam("date", "2024-02").when()
				.post("/getepayPortal/eproRest/eproGetDynamicSalaryDetails").then().assertThat().statusCode(200)
				.extract().response().asString();

		System.out.println("Response : " + response);

	}
	
	public void EnterCTC() throws FileNotFoundException {
		
		String body = "{mid: \"xtFGO11w3whEwKvVNLWHrw==\", ctc: \"4000\", grade: \"HR\"}";
		
		
		String response = given().spec(Utils.requestSpecification().body(body))
				.when().post("/getepayPortal/eproRest/eproCalculatePerks").then().extract().response().asString();
		
		System.out.println(response);
		

//	http://164.52.216.34:8085/getepayPortal/eproRest/eproSaveDynamicSalaryDetails?data=eyJlaWQiOiI4NTAiLCJncmFkZSI6IkhSIiwiY3RjIjoiMTAwMCIsIm1pZCI6Inh0RkdPMTF3M3doRXdLdlZOTFdIcnc9PSIsImRhdGUiOiIyMDI0LTAyIiwiaWQiOiIiLCJwZXJrcyI6eyJCQVNJQyI6IjQwMC4wMCIsIlBGIjoiMTAwLjAwIiwiSFJBIjoiMjAwLjAwIiwiVERTIjoiMTAwLjAwIiwiSW5jZW50aXZlcyI6IjEwMC4wMCIsIkJvbnVzIjoiMTAwLjAwIiwiQWRkaXRpb25hbCI6IjEwMC4wMCJ9fQ==
		
	}
		
		
		public void SaveSalarydetails() throws FileNotFoundException {
			
			given().spec(Utils.requestSpecification()).queryParam("data", "encryptedBody").when().
			post("/getepayPortal/eproRest/eproSaveDynamicSalaryDetails").then().assertThat().statusCode(200);
			
			
		}
		
		
	}

