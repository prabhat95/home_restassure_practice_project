package Resources;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Properties;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class Utils {

	public static RequestSpecification requestSpecification() throws FileNotFoundException {

		PrintStream log = new PrintStream(new FileOutputStream("logg.text"));

		RequestSpecification req = new RequestSpecBuilder().setBaseUri(globalProperty("baseURI"))
				.addHeader("Content-Type", "application/json").addFilter(RequestLoggingFilter.logRequestTo(log))
				.addFilter(ResponseLoggingFilter.logResponseTo(log)).build();

		return req;
	}

//		.body("{\"encryptedRequest\": \"" + encryptedBody + "\"}")

	public static RequestSpecification requestSpecification_Onboarding(String body) throws FileNotFoundException {

		PrintStream log = new PrintStream(new FileOutputStream("logg.text"));

		String encryptedBody = Encrypt_Decrypt_body.encrypt(body);
		System.out.println("encryptedBody : " + encryptedBody + "\n");

		RequestSpecification req = new RequestSpecBuilder().setBaseUri(globalProperty("baseURI"))
				.setContentType(ContentType.TEXT).addHeader("Content-type", "application/json")
				.setBody("{\"encryptedRequest\": \"" + encryptedBody + "\"}")
				.addFilter(RequestLoggingFilter.logRequestTo(log)).addFilter(ResponseLoggingFilter.logResponseTo(log))
				.build();

		return req;

//		.body("{\"encryptedRequest\": \"" + encryptedBody + "\"}")
	}

	public static String globalProperty(String key) {

		Properties prop = null;
		FileInputStream file = null;
		try {
			file = new FileInputStream("config.properties");

			prop = new Properties();

			prop.load(file);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String value = prop.getProperty(key);

		return value;

	}

}
