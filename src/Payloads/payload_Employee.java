package Payloads;

public class payload_Employee {

	static String eid = "682";
	static String date1 = "2024-02-21";
	static String date2 = "2024-02-24";

	static String dayid = "30711";
	public static String attendance = "PL";

	public static String EmployeeAttendanceBody() {
		return "{\"fromDate\":\"" + date1 + "\",\"toDate\":\"" + date2 + "\",\"empId\":\"" + eid
				+ "\",\"mid\":\"xtFGO11w3whEwKvVNLWHrw==\"}";
		
	
	}

	public static String UpdateEmployeeAttendance() {
		return "{\r\n" + "    \"id\": \"" + dayid + "\",\r\n" + "    \"mid\": \"xtFGO11w3whEwKvVNLWHrw==\",\r\n"
				+ "    \"empId\": \"" + eid + "\",\r\n" + "    \"attendanceType\": \"" + attendance + "\"\r\n" + "}";
	}

}
