package Payloads;

public class Payload {

	static String eid = "590";
	static String date1 = "2024-02-21";
	static String date2 = "2024-02-29";

	static String dayid = "31862";
	static String attendance = "A";

	public static String EmployeeAttendanceBody() {
		return "{\"fromDate\":\"" + date1 + "\",\"toDate\":\"" + date2 + "\",\"empId\":\"" + eid
				+ "\",\"mid\":\"6irdwDznArIvUbS3POLsqg==\"}";
	}

	public static String UpdateEmployeeAttendance() {
		return "{\r\n" + "    \"id\": \"" + dayid + "\",\r\n" + "    \"mid\": \"6irdwDznArIvUbS3POLsqg==\",\r\n"
				+ "    \"empId\": \"" + eid + "\",\r\n" + "    \"attendanceType\": \"" + attendance + "\"\r\n" + "}";
	}

}
