package Onboarding;

import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

import Resources.Encrypt_Decrypt_body;
import io.restassured.RestAssured;

public class filterAPI {

	
	String body ="{\r\n"
			+ "  \"soId\": \"66066\",\r\n"
			+ "  \"bankId\": \"193\",\r\n"
			+ "  \"branchId\": \"4632\",\r\n"
			+ "  \"pageNumber\": \"1\",\r\n"
			+ "  \"status\": \"null\",\r\n"
			+ "  \"fromDate\": \"2024-03-01\",\r\n"
			+ "  \"toDate\": \"2024-03-30\",\r\n"
			+ "  \"searchData\": \"\",\r\n"
			+ "  \"sortBy\": \"\"\r\n"
			+ "}\r\n"
			+ "";
	
	
	@Test
	public void merchantReport() {
		
		String encryptedBody = Encrypt_Decrypt_body.encrypt(body);
		System.out.println("EncryptedBody : "+ encryptedBody);
		System.out.println(" ");
		
		RestAssured.baseURI = "http://164.52.216.34:8085";
		
	String response = given().header("Content-Type", "application/json").body("{\"encryptedRequest\": \"" + encryptedBody + "\"}").log().all().
		when().post("/onBoardingApi/onboard/getAll").then().extract().response().asString();
		
	
		System.out.println(" ");
		System.out.println("Response "+ response);
		
	}
	
	
	
	
}
