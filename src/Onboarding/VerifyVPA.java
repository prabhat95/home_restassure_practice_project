package Onboarding;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import Payloads.onb_payload;
import Resources.Encrypt_Decrypt_body;
import Resources.Utils;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class VerifyVPA {
	
	public static String otp = null;

	

	@Test
	public void verfiyVpa() throws IOException {

//		String encryptedBody = Encrypt_Decrypt_body.encrypt(body);

		RestAssured.baseURI = "http://164.52.216.34:8085";

		String Response = given().spec(Utils.requestSpecification_Onboarding(onb_payload.VerfiyVPA())).log().all().when()
				.post("/onBoardingApi/onboard/verifyVpa").then().extract().response().asString();

		System.out.println("Response : " + Response +"\n");

		JsonPath res = new JsonPath(Response);
		String encryptioin = res.getString("encryptedResponse");

		String decryptedBody = Encrypt_Decrypt_body.decrypt(encryptioin);
		System.out.println("DecryptedBody "+ decryptedBody +"\n");
		JsonPath resp = new JsonPath(decryptedBody);
		 otp = resp.getString("otp");
		System.out.println(otp + "\n");
		
		Save_Otp_to_file(otp);

		Registration_with_new_Mobile_Number_Response_Validation(Response,otp);

	}
	
	

	public void Registration_with_new_Mobile_Number_Response_Validation(String Response, String otp) {

		JsonPath response = new JsonPath(Response);

		String message = response.getString("message");

		int status = response.get("status");
		System.out.println(message + "\n" + status+ "\n");

		Assert.assertEquals(status, 200);

		Assert.assertEquals(message, "Your One Time Password is: "+otp+".- Futuretek");

	}

	public void Allready_Register_Mobile_Number_Api_Response_Validation(String Response) {

		JsonPath response = new JsonPath(Response);

		String message = response.getString("message");

		int status = response.get("status");
		System.out.println(message +"\n" + status + "\n");

		Assert.assertEquals(status, 400);
		Assert.assertEquals(message, "Mobile number already registered!"+ "\n");
	}
	
	
	public void Save_Otp_to_file(String otp) throws IOException {
		
		
		FileWriter file = new FileWriter("C:\\Users\\KPK\\git\\repositoryApi\\MyRestAssureProject\\src\\Resources\\merchantOtp.txt");
		
		file.write(otp);
				
		file.close();
		
	}

}
