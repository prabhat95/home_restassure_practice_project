package Onboarding;

import static io.restassured.RestAssured.given;

import java.io.FileNotFoundException;

import org.testng.annotations.Test;

import Payloads.onb_payload;
import Resources.Utils;

public class VerifyOTP {


	@Test
	public void verfiyOtp() throws FileNotFoundException {
		
		

		String response = given().spec(Utils.requestSpecification_Onboarding(onb_payload.verifyOtp_api())).
				when().post("/onBoardingApi/onboard/verifyOtp").
				then().extract().response().asString();

		System.out.println("Response : " + response + "\n");
	}

}
