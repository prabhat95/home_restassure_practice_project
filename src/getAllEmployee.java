import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.filter.session.SessionFilter;

public class getAllEmployee {

	@Test(priority = 1)
	public void login() {

		RestAssured.baseURI = "http://164.52.216.34:8085";
		given().log().all().formParam("username", "test_merchant").formParam("password", "Test@123")
				.header("Content-Type", "application/json").when().post("/getepayPortal/login").then().log().all()
				.assertThat().statusCode(302);

	}

	SessionFilter session = new SessionFilter();
	
	

	@Test(priority = 2)
	public void getEmployeeInfo() {
		

		RestAssured.baseURI = "http://164.52.216.34:8085";

		given().log().all().header("Content-Type", "application/json")
				.header("Authorization", "Basic VGVzdF9tZXJjaGFudDpUZXN0QDEyMw==").queryParam("id", "194")
				.filter(session).when().get("/getepayPortal/epro/eproGetEmployeeAllDetails").then().log().all()
				.assertThat().statusCode(200);

	}
	// http://164.52.216.34:8085/getepayPortal/epro/eproEmployeeRegistration
}
