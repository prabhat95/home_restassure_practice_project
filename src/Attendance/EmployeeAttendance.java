package Attendance;

import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

import Payloads.payload_Employee;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import pojo.attendancehild;
import pojo.employeeAtt;

//post
@Test(priority = 1 , description="post filter employee attendance")
public class EmployeeAttendance {

	//post filter employee attendance
	public void FetchEmployeeAttendance() {
		
		 RequestSpecification Req = new RequestSpecBuilder().setBaseUri("\"http://164.52.216.34:8085\"").
				 addHeader("Content-Type", "application/json").
				 addHeader("Authorization", "Basic VGVzdF9tZXJjaGFudDpUZXN0QDEyMw==").build();

		employeeAtt  response = given().log().all().spec(Req)
				.body(payload_Employee.EmployeeAttendanceBody()).when().post("/getepayPortal/eproRest/eproEmployeeAttendance")
				.as(employeeAtt.class);
		
		
		 for (attendancehild attendanceData : response.getData()) {
		        System.out.println(attendanceData.getId());
		        // Print other fields as needed
		    }
		
		

		
		
		
	}
	
/*
	// post to updated  attendance
	@Test(priority = 2, description="post to updated  attendance")
	public void updateEmployeeAttendance() {

		RestAssured.baseURI = "http://164.52.216.34:8085";
		String response = given().header("Content-Type", "application/json")
				.header("Authorization", "Basic VGVzdF9tZXJjaGFudDpUZXN0QDEyMw==")
				.body(payload_Employee.UpdateEmployeeAttendance()).when()
				.post("/getepayPortal/eproRest/eproEmployeeAttendenceUpdate").then().extract().response().asString();

		JsonPath js = new JsonPath(response);
		System.out.println(response);

		int statusCode = js.getInt("status");
		String validation = js.getString("message");
		
		Assert.assertEquals(statusCode, 200);
		Assert.assertEquals(validation, "Employee attendance updated successfully.");
		System.out.println("Status code is : " + statusCode + "\n" + "validation is : " + validation);

	}

	// get updated attendance
	@Test(priority = 3, description="get updated attendance")
	public void FetchEmployeeUpdatedAttendance1() {

		RestAssured.baseURI = "http://164.52.216.34:8085";
		String response = given().header("Content-Type", "application/json")
				.header("Authorization", "Basic VGVzdF9tZXJjaGFudDpUZXN0QDEyMw==")
				.body(payload_Employee.EmployeeAttendanceBody()).when().post("/getepayPortal/eproRest/eproEmployeeAttendance")
				.then().extract().response().asString();
		
//		assertThat().body("attendanceType", equalTo(Payload.attendance))

		JsonPath js = new JsonPath(response);
		int count = js.getInt("data.size()");

		System.out.println("count : "+ count);

		for (int i = 0; i < count; i++) {

			Object object = js.get("data[" + i + "].id");

			if (object.equals("30711")) {

				Object object2 = js.get("data[" + i + "]");
				
				System.out.println(">>> " + object2.toString());
				
				
			}

		}
	}
	*/

}
